<!DOCTYPE html>
<html>
<head>
	<title>Mediajogger widget layout one</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="lightslider/dist/css/lightslider.min.css">


</head>
<body>

	<script
	src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="../youtubeApi.js"></script>
	<script src="lightslider/dist/js/lightslider.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#autoWidth').lightSlider({
				item:2,
				loop:false,
				vertical:true,
				verticalHeight:310,
				slideMove:2,
				easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
				speed:600,
				pager:false,
				responsive : [
				{
					breakpoint:800,
					settings: {
						item:3,
						slideMove:1,
						slideMargin:6,
					}
				},
				{
					breakpoint:480,
					settings: {
						item:2,
						slideMove:1
					}
				}
				]
			});  
		});
	</script>
</body>
</html>