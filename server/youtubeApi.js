
// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('playerId', {
    // autoplay:0,
    height: '390',
    width: '640',
    videoId: '6r4cKaD71Hk',
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    },
    playerVars: {
      'autoplay': 0,
      'rel': 0,
      'wmode': "transparent",
      'controls': 1,
      'enablejsapi': 1,
      'iv_load_policy': 3,
      'modestbranding': 0,
      'showinfo': 1,
      'autohide': 1,
      //playlist:['aAtkxOpX8Zg','NENDZsGBTeQ','aAtkxOpX8Zg','NENDZsGBTeQ','aAtkxOpX8Zg','NENDZsGBTeQ']
    }
  });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  // event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  // if (event.data == YT.PlayerState.PLAYING && !done) {
  //   setTimeout(stopVideo, 6000);
  //   done = true;
  // }
}

$('.mj-widget-modal').on('click', function () {

  stopVideo();
});

function stopVideo() {

  player.stopVideo();
}