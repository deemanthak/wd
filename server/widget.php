<!DOCTYPE html>
<html>
<head>
  <title>this is modal</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

</head>
<style type="text/css">
.services-list {
  margin-top: 25%;
}
.services-list div {
  width: 33.33%;
  float: left;
  margin-bottom: 2%;
}
.services-list div img {
  width: auto;
  max-height: 60px;
  display: block;
  margin: 0 auto;
}
.services-list div p {
  text-align: center;
  margin-top: 10px;
  font-size: 14px;
  font-weight: 500;
  /*font-family: Roboto-Medium;*/
}
.services-img img {
  width: 90%;
  margin-top: 24%;
}
/* Float */
.hvr-float {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: perspective(1px) translateZ(0);
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px transparent;
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;
  -webkit-transition-property: transform;
  transition-property: transform;
  -webkit-transition-timing-function: ease-out;
  transition-timing-function: ease-out;
}
.hvr-float:hover, .hvr-float:focus, .hvr-float:active {
  -webkit-transform: translateY(-8px);
  transform: translateY(-8px);
}
</style>
<body>


  <!-- <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> -->
   <!-- <div class="modal-dialog modal-lg" style="width: 80%;height: 95%;margin: auto auto auto;margin-top: 10px;margin-left: auto;" role="document"> -->
    <div class="" style="border-radius:10px;font-family: 'Montserrat', sans-serif;height: 100%;">
     <div class="modal-header" style="box-shadow: 0px 1px 40px rgba(0,0,0,.2);border-bottom: 1px solid red;">
      <div class="row" style="margin-top:10px;">
       <div class="col-md-2 col-sm-2 col-lg-2">
        <img class="center" src="http://mediajogger.com/images/logo/MediajoggerLogo.png" alt="mediajoggerLogo" style="width:150px;margin-bottom:10px;">
      </div>
      <div class="col-md-9 col-sm-9  col-lg-9">
        <div class="services-list" style="margin-top:0px !important;">
         <div class="hvr-float  categoryItem" style="width:25% !important;margin-bottom:0px !important;" data-id="6r4cKaD71Hk"
         data-title="Video production"
         data-desc="We&#39;re fully equipped to tell brand stories through TV commercials, corporate profiles, documentaries
         ,event coverage&#39;s, interviews, short films, product demonstrations, green screen recordings and many
         other forms of visualizing conversations">
         <img src="images/icons/video-production.png">
         <p>Video production</p>
       </div>
       <div class="hvr-float categoryItem" style="width:25% !important;margin-bottom:0px !important;" data-id="lsovacyOUWo"
       data-title="Photography"
       data-desc="Our photography services range from event coverage&#39;s to product shoots, destination photography,
       office staff portraits , product placements, models shots and design concept shoots.">
       <img src="images/icons/photography.png">
       <p>Photography</p>
     </div>
     <div class="hvr-float categoryItem" style="width:25% !important;margin-bottom:0px !important;" data-id="hS725cWecic"
     data-title="3D & Motion Graphics"
     data-desc="We help companies simplify complex information by telling stories with an appealing visual impact.
     These animated stories could be about products and services, financial information or simply converting
     a long power point presentation or a publication in to a 2 minute video">
     <img src="images/icons/3D-and-motion-graphic-videos.png">
     <p>3D & Motion Graphics</p>
   </div>
   <div class="hvr-float categoryItem" style="width:25% !important;margin-bottom:0px !important;" data-id="DxsgiGb6yKw"
   data-title="Designing"
   data-desc="We design all varieties of visuals layouts from basic brochures, advertisements, corporate profile to
   complex concepts, websites and apps">
   <img class="design" src="images/icons/designing.png">
   <p>Designing <?php echo $_GET['companyId'];?></p>
 </div>
</div>
</div>
<div class="col-md-1 col-sm-1  col-lg-1">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
</div>
</div>
<div class="modal-body" style="padding:35px;height:80%;">
  <!-- <h4>Video Production</h4> -->
  <div class="row">
   <div class="col-md-7 col-lg-7">
    <div class="row" style="margin: 10px;">
     <div id="playerId" style="width: 100%;margin: 5px;border-radius: 10px;height: 325px;" ></div>
   </div>
   
   
   <div class="row your-class" style="margin: 10px;">
     <div class="col-md-3 col-lg-3 col-sm-6 ">
      <img src="https://img.youtube.com/vi/tUvsxhNZ-rc/mqdefault.jpg" data-id="tUvsxhNZ-rc"   class="playlistImages" alt="" style="width:100%;border-radius:5px;cursor:pointer;"
      data-title="The role of a digital content agency"
      data-desc="As an digital content agency, we provide content solutions by analyzing gaps in digital conversations.
      We have streamlined the process of convenient content creation from the first call to the final product"
      >
      <p style="margin-top:3px;">The role of a digital content agency</p>
    </div>
    <div class="col-md-3 col-lg-3 col-sm-6 ">
      <img src="https://img.youtube.com/vi/6O58QI-3UPY/mqdefault.jpg" data-id="6O58QI-3UPY" class="playlistImages" alt="" style="width:100%;border-radius:5px;cursor:pointer;"
      data-title="Producing explainer videos"
      data-desc="We strongly believe that information should be simplified ! Producing explainer videos are amongst the
      most effective modes of communicating details to your stakeholders"
      >
      <p style="margin-top:3px;">Producing explainer videos</p>
    </div>
    <div class="col-md-3 col-lg-3 col-sm-6 ">
      <img src="https://img.youtube.com/vi/ObwxQmIB1fg/mqdefault.jpg" data-id="ObwxQmIB1fg" class="playlistImages" alt="" style="width:100%;border-radius:5px;cursor:pointer;"
      data-title="Visualizing web content"
      data-desc="Websites are often cluttered with too much text and images, as a result web visitors often struggle to
      understand how your business works. Visualizing web content makes it much easier for anybody to
      understand your brand stories"
      >
      <p style="margin-top:3px;">Visualizing web content</p>
    </div>
    <div class="col-md-3 col-lg-3 col-sm-6">
      <img src="https://img.youtube.com/vi/v4LKhevLmZE/mqdefault.jpg" data-id="v4LKhevLmZE" class="playlistImages" alt="" style="width:100%;border-radius:5px;cursor:pointer;"
      data-title="360 event coverage"
      data-desc="We understand the importance and impact of corporate events . Our 360 event content solutions help
      organizations to derive lasting results from events"
      >
      <p style="margin-top:3px;">360 event coverage</p>
    </div>
  </div>
</div>
<div class="col-md-5 col-lg-5">
  <div class="row" style="margin:10px;">
   <div class="col-md-8 col-lg-8 col-sm-8">
    <h3  id="Vtitle">Video production</h3>
  </div>
  <div class="col-md-4 col-lg-4 col-sm-4">
    <img id="Vicon" src="images/icons/video-production.png" style="max-height:100px;">
  </div>
</div>
<div class="row" style="margin:20px;">
 <div style="border-radius:10px;border:1px solid red;padding:20px;text-align:left;">
  <p style="" id="Vdesc">We&#39;re fully equipped to tell brand stories through TV commercials, corporate profiles, documentaries
   ,event coverage&#39;s, interviews, short films, product demonstrations, green screen recordings and many
   other forms of visualizing conversations
 </p>
 <!-- <button type="button" class="btn btn-danger" name="button">More</button> -->
</div>
</div>
<div class="row" style="margin:20px;">
 <div class="sio-icon">
  <span>
    <a class="f" href="https://www.facebook.com/mediajoggersl/"  target="_blank"></a>
  </span>
  <span>
    <a class="v" href="https://vimeo.com/user31620173/videos"  target="_blank"></a>
  </span>
  <span>
    <a class="i" href="https://www.instagram.com/mediajogger_sl"  target="_blank"></a>
  </span>
  <span>
    <a class="l" href="https://www.linkedin.com/company/mediajogger"  target="_blank"></a>
  </span>
</div>
</div>
</div>
</div>
<div class="row">
 <p style="color:red;float:right;">Powered by <strong><a target="_blank" href="http://easyviewmode.com" style="color:red;">Easy View Mode</a></strong></p>
</div>
</div>
</div>
<!-- </div> -->
<!-- </div> -->
<!-- <script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script> -->

        <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
        
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script>

          var company_id = "<?php echo $_GET['companyId']; ?>";
          $.ajax({
            type: 'POST',
            url:"getCompanyData.php",
            data: {'com_id': company_id},
            dataType: 'json',
            success: function(data){
              console.log(data);
            }
          });

//   $.ajax({
//     url:"getCompanyData.php",
//     type: 'POST',
//     dataType: 'json',
//     data: {'com_id': company_id},
//     success:function(data){
// alert();
//     }

//   });

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('playerId', {
    // autoplay:0,
    height: '390',
    width: '640',
    videoId: '6r4cKaD71Hk',
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    },
    playerVars: {
      'autoplay': 0,
      'rel': 0,
      'wmode': "transparent",
      'controls': 1,
      'enablejsapi': 1,
      'iv_load_policy': 3,
      'modestbranding': 0,
      'showinfo': 1,
      'autohide': 1,
      //playlist:['aAtkxOpX8Zg','NENDZsGBTeQ','aAtkxOpX8Zg','NENDZsGBTeQ','aAtkxOpX8Zg','NENDZsGBTeQ']
    }
  });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
  // event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
  // if (event.data == YT.PlayerState.PLAYING && !done) {
  //   setTimeout(stopVideo, 6000);
  //   done = true;
  // }
}

$('.mj-widget-modal').on('click', function () {

  stopVideo();
});

function stopVideo() {

  player.stopVideo();
}

$(document).ready(function(){

  // $('#videoModal').modal('show');

});

$('.playlistImages').on('click',function(){
  var id = $(this).data('id');
  var title = $(this).data('title');
  var desc = $(this).data('desc');
  // var icon = $(this).find('img').attr('src');
  var icon = "";
  $('#Vtitle').html(title);
  $('#Vdesc').html(desc);
  $('#Vicon').attr('src',icon);

  player.loadVideoById(id, 0, 'default');
});


$('.categoryItem').on('click',function(){
  // alert();
  var id = $(this).data('id');
  var title = $(this).data('title');
  var desc = $(this).data('desc');
  var icon = $(this).find('img').attr('src');

  $('#Vtitle').html(title);
  $('#Vdesc').html(desc);
  $('#Vicon').attr('src',icon);

  player.loadVideoById(id, 0, 'default');
});

$('.clickIcons').on('click',function(){
  // alert();
  var id = $(this).data('id');
  var title = $(this).find('p').html();
  var desc = $(this).data('desc');
  var icon = $(this).find('img').attr('src');
// id="3tmd-ClpJxA";
$('#Vtitle').html(title);
$('#Vdesc').html(desc);
$('#Vicon').attr('src',icon);
$('#videoModal').modal("show");
player.loadVideoById(id, 0, 'default');
});

$('.clickToPop').on('click',function(e){
  e.preventDefault();
  var id = $(this).data('id');
  var title = $(this).data('title');
  var desc = $(this).data('desc');
  var icon = "";
  $('#Vtitle').html(title);
  $('#Vdesc').html(desc);
  $('#Vicon').attr('src',icon);
  $('#videoModal').modal("show");
  player.loadVideoById(id, 0, 'default');

});
</script>

<link rel="stylesheet" type="text/css" href="layouts/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="layouts/slick/slick-theme.css"/>
<!-- <style type="text/css">
  .slick-prev{
    background-color: 
  }
</style> -->
<script type="text/javascript" src="layouts/slick/slick.js"></script>

<script>
  $('.your-class').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
    // vertical:true
    // arrows:true
  });
</script>


</body>
</html>