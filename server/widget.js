(function () {
    var jQuery;
    var serverFQDN = 'http://localhost/wd/server/';
    // var timeoutId, options, container;
    var this_username, this_com_id;
    // var mj_playerType;
    // var mj_category_type;

    if (!window.MediajoggerWidget)
        window.MediajoggerWidget = {};
    MediajoggerWidget.Widget = function (opts) {
        options = opts;
        if (!options.buttonText) {
            options.buttonText = 'This text is configurable';
        }
        this_username = options.userName;
        this_com_id = options.com_id;
        // mj_playerType = options.player_type;
        // mj_category_type = options.category_type;
        container = '.widget-body';
    };
    function init() {
        if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.11.2') {
            console.log('we need to load jQuery');
            var script_tag = document.createElement('script');
            script_tag.setAttribute("type", "text/javascript");
            script_tag.setAttribute("src", "https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js");
            (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
            if (script_tag.attachEvent) {
                //console.log('oh cool, this is IE');
                script_tag.onreadystatechange = function () { // for IE
                    if (this.readyState == 'complete' || this.readyState == 'loaded') {
                        this.onreadystatechange = null;
                        scriptLoadHandler();
                    }
                };
            } else {
                script_tag.onload = scriptLoadHandler;
            }
        } else {
            jQuery = window.jQuery;
            console.log('jQuery already exists on page');
            main();
        }
    }

    function scriptLoadHandler() {
        jQuery = window.jQuery.noConflict();
        console.log('jQuery is now loaded');
        main();
    }

    function main() {
        jQuery(document).ready(function () {
            console.log('MAin script attached and working fine');
            jQuery('head').append('<link href="' + serverFQDN + '/widget.css" rel="stylesheet" type="text/css">');
   
            $('#mj-modal-btn').on('click',function(){
                //add url when click outside of the modal
                // $('#mj-iframe').attr('src','');
                $('#mj-iframe').toggle();
            });

            $('body').on('click','.mj-widget-modal__bg',function(){
                //to stop video when click outside of the modal
                // var src_val = $('#mj-iframe').attr('src');
                $('#mj-iframe').attr('src','');
            });
            $('body').on('click','.mj-float-btn',function(){
                //to stop video when click outside of the modal
                var src_val = "http://localhost/wd/server/widget.php?companyId='"+this_username+"'";
                 var src_val ="http://localhost/wd/server/layouts/1/layout1.html";
                $('#mj-iframe').attr('src',src_val);
            });

        });




        if (jQuery(container).size() === 0) {
//                jQuery('body').append('<div class="widget-body"></div>');

                    jQuery('.mjogger_wid').html('<label class="mj-float-btn" for="mj-widget-modal-1"> </label>');
                    jQuery('.mjogger_wid').append(' <input class="mj-widget-modal-state" id="mj-widget-modal-1" type="checkbox" />' +
                        '<div class="mj-widget-modal">' +
                        '<label class="mj-widget-modal__bg" for="mj-widget-modal-1"></label>' +
                        '<div class="mj-widget-modal__inner">' +
                        // '<div class="loader" id="widgetLoader">Loading...</div>'+
                        // '<iframe id="mj-iframe" src="http://localhost/wd/server/widget.php?companyId='+this_username+'"></iframe>'+

                         '<iframe id="mj-iframe" src=""></iframe>'+
                        // '<div style="width:98%;height:90%;" id="innerModalData">' +
                        
                        // '<label class="mj-widget-modal__close" for="mj-widget-modal-1"></label>' +
                        // '<h2 id="mj-widget-vspace-title">' + "this_username" + ' Visual Space</h2>' +
                        // "category_bar" +
                        // '</div>' +
                        '</div>' +
                        '</div>');
                //alert();

            }
        }

        function getMyVideoData() {

            // var com_id = this_com_id;

            // jQuery.ajax({
            //     url: 'http://localhost/widget/server/widget_submit.php',
            //     method: 'GET',
            //     dataType: 'json',
            //     data: {'com_id': com_id},
            //     success: function (data2) {
            //         var data_array=[];
            //         console.log(data2);
            //         jQuery.each(data2,function(i,e){
            //             data_array.push(data2[i].video_id);
            //         });
            //         set_variables(data_array, com_id,data2);

            //     }
            // });
        }

    // function serverResponse(data) {
    //     jQuery(container + ' button').removeAttr('disabled');
    //     updateStatus(data.message + ' Your ip is ' + data.ip + '. Host site is ' + data.site + '. UA is ' + data.ua);
    // }

    // function updateStatus(msg) {
    //     if (jQuery(container + ' p.message').length === 0) {
    //         jQuery(container).append('<p class="message"></p>');
    //     }
    //     jQuery(container + ' p.message').html(msg);
    // }

    init();
})();

